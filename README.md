# Основной бекенд АС Гемба

## Запуск

```
npm install
node gemba_back.js
```
наполнить БД, можно с помощью скриптов из populate.js

## О системе
Как БД пока используется mongoDB, все двигается в сторону микросервисов, в данный момент - генерация экселя,
это уже отдельный сервис на яве. (https://sbtatlas.sigma.sbrf.ru/stash/projects/CGIS/repos/gemba_exel_service/browse)
На данный момент новые фичи будут сосредоточены в ветке stage2.
Структура мероприятия снова не плоская, и будет пилиться в соотвествии со схемой:

![N|Solid](https://sbtatlas.sigma.sbrf.ru/stash/projects/CGIS/repos/gemba_back/raw/info/gemba_event.png)

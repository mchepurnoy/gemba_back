const express = require('express');

const dbs = require('../db/service.js');
const filters = require('../helpers/filters.js');
const routerOps = require('../helpers/routerOperations.js');

const r = express.Router();

r.post('/login', (req, res) => {
    if (req.body && req.body.username && req.body.password) {
        dbs.findOne('User', {
            username: req.body.username,
            password: req.body.password
        }).then(data => {
            if (data && data.token) {
                res.json(data);
            } else {
                throw {code: 'invalid_session'};
            }
        }).catch(ex => {
            res.json({error: ex})
        });
    } else {
        res.send({error: {code: 'invalid_data'}});
    }
});

r.get('/check', (req, res) => {
    if (req.get('gistoken')) {
        dbs.findOne('User', {token: req.get('gistoken')})
            .then(data => {
                if (data && data.token) {
                    res.json(data);
                } else {
                    throw {code: 'invalid_session'};
                }
            })
            .catch(ex => {
                res.json({error: ex})
            });
    } else {
        res.send({error: {code: 'invalid_data'}});
    }
});

r.get('/missions', routerOps.sessionCheck, (req, res) => {
    dbs.find('Event',
        {"missions": {$elemMatch: {executors: req.user._id}}},
        req.query.limit || 1000)
        .then(data => {
            res.json(filters.autoFilterList(data));
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

module.exports = r;
const express = require('express');
const _ = require('lodash');

const dbs = require('../db/service.js');
const filters = require('../helpers/filters.js');
const routerOps = require('../helpers/routerOperations.js');
const validation = require('../helpers/validators.js');
//const schemas = require('../db/schemas.js');

const r = express.Router();
r.use(routerOps.sessionCheck);

r.post('/create', (req, res) => {
    const body = filters.addCreator(req.body, req.user);
    dbs.create('Event', body)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        })
});

r.get('/:id', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            data.permissions = validation.findRole(data, req.user.token);
            res.json(filters.autoFilter(data));
            //res.json({data:data, user:req.user, perms:validation.findRole(data, req.user.token)})
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

r.delete('/:id', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            if (data.creator && req.user.token == data.creator.token) {
                return dbs.remove('Event', {_id: req.params.id});
            } else {
                throw {code: 'no_access'};
            }
        })
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

r.put('/:id', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            if (data.creator && req.user.token == data.creator.token) {
                return dbs.edit('Event', {_id: req.params.id},
                    _.omit(req.body), ['creator', '_id']);
            } else {
                throw {code: 'no_access'};
            }
        })
        .then(data => {
            res.json(filters.autoFilter(data));
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

function checkEventStatus(event, upd) {
    if (event.missions && event.missions.length) {
        if (upd.id == '_new_') {
            event.missions.push({
                status: upd.status
            });
        } else if (upd.status == '_del_') {
            event.missions = event.missions.filter(m => {
                return m._id != upd.id
            });
        }else {
            for (let i in event.missions) {
                if (event.missions[i]._id == upd.id) {
                    event.missions[i].status = upd.status;
                    break;
                }
            }
        }
        var status = 'new';
        var done = true;
        event.missions.forEach(m => {
            if (m.status == 'working') {
                status = 'working';
            }
            if (m.status != 'done') {
                done = false;
            }
        });
        return (done) ? 'done' : status;
    } else {
        return 'new';
    }
}

//body вида {"status": "done"}
r.put('/executor/edit/:id/:mid', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            const permissions = validation.findRole(data, req.user.token);
            //console.log(permissions);
            if (permissions.missions[req.params.mid] == 'executor' ||
                permissions.missions[req.params.mid] == 'creator' ||
                permissions.event == 'creator') {
                const status = checkEventStatus(data, {
                    id: req.params.mid,
                    status: req.body.mission.status
                });
                if (req.body.mission && req.body.mission.status) {
                    return {'$set': {
                        'missions.$.status': req.body.mission.status,
                        'status': status
                    }}
                } else {
                    throw {code: 'invalid_data'};
                }
            } else {
                throw {code: 'no_access'};
            }
        })
        .then(data => {
            return dbs.edit('Event', {_id: req.params.id, 'missions._id': req.params.mid}, data);
        })
        .then(data => {
            res.json(filters.autoFilter(data));
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

const allowedCols = new Set(['missions', 'causes', 'problems', 'places', 'processes']);

//:id - ид события
//{"type": "member", "id":""}
r.put('/member/add/:id', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            const permissions = validation.findRole(data, req.user.token);
            if (permissions.event == 'member' || permissions.event == 'creator') {
                if (req.body && req.body.type) {
                    switch (req.body.type) {
                        case 'member':
                            return dbs.findOne('User', {_id: req.body.id});
                        case 'problem':
                            return dbs.edit('Event', {_id: req.params.id},
                                {$addToSet: {
                                    problems: Object.assign(
                                        {creator: req.user._id},
                                        req.body.problem
                                    )
                            }});
                        case 'cause':
                            return dbs.edit('Event', {_id: req.params.id},
                                {$addToSet: {
                                    causes: Object.assign(
                                        {creator: req.user._id},
                                        req.body.cause
                                    )
                                }});
                        case 'mission':
                            const status = checkEventStatus(data, {
                                id: '_new_',
                                status: req.body.mission.status
                            });
                            return dbs.edit('Event', {_id: req.params.id},
                                {$addToSet: {
                                    missions: Object.assign(
                                        {creator: req.user._id},
                                        req.body.mission
                                    )
                                }, 'status': status});
                        case 'place':
                            return dbs.findOne('Depart', {_id: req.body.id});
                        case 'process':
                            return dbs.findOne('Process', {_id: req.body.id});
                    }
                } else {
                    throw {code: 'invalid_data'};
                }
            } else {
                throw {code: 'no_access'};
            }
        })
        .then(data => {
            switch (req.body.type) {
                case 'member':
                    if (data.token) {
                        return dbs.edit('Event', {_id: req.params.id},
                            {$addToSet: {members: data._id}});
                    } else {
                        throw {code: 'no_such_user'};
                    }
                case 'problem':
                    return data;
                case 'cause':
                    return data;
                case 'mission':
                    return data;
                case 'place':
                    if (data.attributes.DEPART_ADRES) {
                        return dbs.edit('Event', {_id: req.params.id},
                            {$addToSet: {places: {
                                doc: data._id,
                                creator: req.user._id
                        }}});
                    } else {
                        throw {code: 'no_such_place'};
                    }
                case 'process':
                    if (data.Code) {
                        return dbs.edit('Event', {_id: req.params.id},
                            {$addToSet: {processes: {
                                doc: data._id,
                                creator: req.user._id
                            }}});
                    } else {
                        throw {code: 'no_such_process'};
                    }
                default:
                    throw {code: 'invalid_data'};
            }
        })
        .then(data => {
            res.json(filters.autoFilter(data));
        })
        .catch(ex => {
            res.json({error: ex})
        });
});

//{"in": "causes", "id":""}
r.put('/member/remove/:id', (req, res) => {
    if (req.body.in && req.body.id && allowedCols.has(req.body.in)) {
        var queryFind = {_id: req.params.id};
        queryFind[req.body.in] = {$elemMatch: {_id: req.body.id}};
        dbs.findOne('Event', queryFind)
            .then(data => {
                const permissions = validation.findRole(data, req.user.token);
                if (permissions[req.body.in] &&
                    permissions[req.body.in][req.body.id] == 'creator') {
                    const status = checkEventStatus(data, {
                        id: req.body.id,
                        status: '_del_'
                    });
                     var queryDel = {
                        _id: req.params.id,
                        '$pull': {},
                         'status': status
                    };
                    queryDel['$pull'][req.body.in] = {_id: req.body.id};

                    return dbs.edit('Event', {_id: req.params.id}, queryDel);
                } else {
                    throw {code: 'no_access'};
                }
            })
            .then(data => {
                res.json(filters.autoFilter(data));
            })
            .catch(ex => {
                res.json({error: ex})
            });
    } else {
        res.json({code: 'invalid_data'})
    }
});

/*r.put('member/edit/:id', (req, res) => {
    dbs.findOne('Event', {_id: req.params.id})
        .then(data => {
            const permissions = validation.findRole(data, req.user.token);
            if (permissions.event == 'member' || permissions.event == 'creator') {
                if (req.body && req.body.type) {
                    switch (req.body.type) {
                        case 'mission':
                            break;
                        default:
                            throw {code: 'invalid_data'};
                    }
                }
            }
        })
        .catch(ex => {
            res.json({error: ex})
        });
});*/

module.exports = r;
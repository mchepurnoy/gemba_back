const express = require('express');

const dbs = require('../db/service.js');
const filters = require('../helpers/filters.js');
const routerOps = require('../helpers/routerOperations.js');
//const validation = require('../helpers/validators.js');
const searchFilterSrv = require('../helpers/searchFilterService.js');

const r = express.Router();
r.use(routerOps.sessionCheck);

r.get('/places', (req, res) => {
    dbs.find('Depart', {}, req.query.limit || 1000)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

r.get('/gemba', (req, res) => {
    var query = {places: {"$gt": []}};
    if (req.query.all) {
        query = {};
    }
    dbs.find('Event', query, req.query.limit || 1000)
        .then(data => {
            res.json(filters.autoFilterList(data));
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

r.post('/gemba/filter',
    (req, res, next) => {
        searchFilterSrv.getEventFilterQuery(req.body, query => {
            if (query.error) {
                res.json(query);
            } else {
                req.filterQuery = query;
                next();
            }
        });
    },
    (req, res) => {
        dbs.find('Event', req.filterQuery, req.query.limit || 1000)
            .then(data => {
                res.json(filters.autoFilterList(data));
            })
            .catch(ex => {
                res.json({error: ex});
            });
});

module.exports = r;
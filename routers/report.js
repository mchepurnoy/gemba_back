const express = require('express');
const moment = require('moment');

const routerOps = require('../helpers/routerOperations.js');
const reqMaker = require('../helpers/reqMaker.js');
const dbs = require('../db/service.js');
const filters = require('../helpers/filters.js');

const r = express.Router();
//r.use(routerOps.sessionCheck);

const arrProps = ['missions', 'causes', 'problems', 'places', 'processes'];
function prepareEvent(ev) {
    const maxColH = Math.max(...arrProps.map(val => {
        return (ev[val] && ev[val].length) ? ev[val].length : 0;
    }));
    var ret = [];
    for (let i = 0; i < maxColH; i++) {
        ret.push([ev._id]);
    }
    ret[0][1] = moment(ev.date).locale('ru').format('LLLL');
    ev.places.forEach((p, i) => {
        ret[i][2] = p.doc.attributes.DEPART_ADRES;
    });
    ev.processes.forEach((p, i) => {
        ret[i][3] = p.doc.Code + '. ' + p.doc.Process;
    });
    ev.problems.forEach((p, i) => {
        ret[i][4] = p.text;
    });
    ev.causes.forEach((p, i) => {
        ret[i][5] = p.text;
    });
    ev.missions.forEach((p, i) => {
        ret[i][6] = p.description;
        ret[i][7] = moment(p.date).locale('ru').format('LLLL');
        ret[i][8] = p.executors.map(u => {return u.username}).join(' ');
    });
    return ret;
}
function prepareData(data) {
    return {
        leader: "_",
        subdivision: "_",
        bank: "sberbank",
        data: [].concat(...data.map(prepareEvent))
    };
}

r.get('/exel', (req, res) => {
    dbs.find('Event', {places: {"$gt": []}}, req.query.limit || 1000)
        .then(data => {
            return prepareData(filters.autoFilterList(data));
        })
        .then(data => {
            return reqMaker.makeReq(data, '/exel');
        })
        .then(data => {
            res.header("Content-Disposition", "filename=" + "report.xlsx");
            res.set('content-type', 'application/octet-stream');
            res.end(data, 'binary');
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

module.exports = r;
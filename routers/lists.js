const express = require('express');
//const _ = require('lodash');

const dbs = require('../db/service.js');
const r = express.Router();
const filters = require('../helpers/filters');
const searchFilterSrv = require('../helpers/searchFilterService.js');

r.get('/processes/:filter', (req, res) => {
    dbs.find('Process',
        searchFilterSrv.searchFilters.process(req.params.filter),
        req.query.limit || 10)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        });
});
r.get('/processes', (req, res) => {
    dbs.find('Process', {},
        req.query.limit || 1000)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

r.get('/departs/:filter', (req, res) => {
    dbs.find('Depart',
        searchFilterSrv.searchFilters.depart(req.params.filter),
        req.query.limit || 10)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        });
});
r.get('/departs', (req, res) => {
    dbs.find('Depart', {},
        req.query.limit || 1000)
        .then(data => {
            res.json(data);
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

r.get('/users/:filter', (req, res) => {
    dbs.find('User',
        searchFilterSrv.searchFilters.user(req.params.filter),
        req.query.limit || 10)
        .then(data => {
            res.json(filters.filterUsers(data));
        })
        .catch(ex => {
            res.json({error: ex});
        });
});
r.get('/users', (req, res) => {
    dbs.find('User', {}, req.query.limit || 10)
        .then(data => {
            res.json(filters.filterUsers(data));
        })
        .catch(ex => {
            res.json({error: ex});
        });
});

module.exports = r;
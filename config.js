module.exports = {
    host: '127.0.0.1',
    //host: '138.201.170.156', //Funtom
    port: 3000,
    //port: 3012, //Funtom
    DB: {
        host: '127.0.0.1',
        port: 27017,
        name: 'gemba3'
        //name: 'gemba2' //Funtom
    }
};
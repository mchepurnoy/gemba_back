process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const express = require('express');
const cookieParser = require('cookie-parser');

const conf = require('./config.js');
const routerOps = require('./helpers/routerOperations.js');
const app = express();

app.use(routerOps.jsonParse);
app.use(cookieParser());
app.use(routerOps.setCORSReq);

const eventRouter = require('./routers/event.js');
app.use('/event', eventRouter);

const mapRouter = require('./routers/map.js');
app.use('/map', mapRouter);

const userRouter = require('./routers/user.js');
app.use('/user', userRouter);

const listRouter = require('./routers/lists.js');
app.use('/list', listRouter);

const reportRouter = require('./routers/report.js');
app.use('/report', reportRouter);

app.listen(conf.port, conf.host, () => {
    console.log('running on ' + conf.host + ':' + conf.port);
});
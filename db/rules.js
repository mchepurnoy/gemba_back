const Joi = require('joi');

const causeRules = {
    creator: Joi.object().keys({
        missions: Joi.array(),
        title: Joi.string(),
        description: Joi.string(),
        files: Joi.array()
    })
};

const misssionRules = {
    creator: Joi.object().keys({
        executor: Joi.string(),
        title: Joi.string(),
        executionDate: Joi.string(),
        solution: Joi.string(),
        files: Joi.array(),
        status: Joi.string(),
        address: Joi.string()
    }),
    executor: Joi.object().keys({
        executionDate: Joi.string(),
        solution: Joi.string(),
        files: Joi.array(),
        status: Joi.string()
    })
};

const problemRules = {
    creator: Joi.object().keys({
        title: Joi.string(),
        description: Joi.string(),
        causes: Joi.array(),
        files: Joi.array()
    })
};

const eventRules = {
    creator: Joi.object().keys({
        params: Joi.object().keys({
            place: Joi.string(),
            process: Joi.string(),
            status: Joi.string(),
            title: Joi.string()
        }),
        description: Joi.string(),
        members: Joi.array(),
        problems: Joi.array(),
        files: Joi.array(),
        address: Joi.string()
    }),
    member: Joi.object().keys({
        description: Joi.string(),
        files: Joi.array(),
        problems: Joi.array(),
        members: Joi.array()
    })
};

const userRules = {
    fieldsGIS: Joi.object().keys({
        username: Joi.string(),
        token: Joi.string(),
        expires: Joi.number(),
        email: Joi.string()
    }).with('username', 'token', 'expires')
};

const member = ['description', 'files', 'members', 'problems', 'address'];
const executor = ['executionDate', 'solution', 'files', 'status'];
function cut (data, props) {
    var res = {};
    props.forEach(p => {
        if (data[p]) {
            res[p] = data[p];
        }
    });
    return res;
}
const cutter = {
    Event: function (data) {
        return cut(data, member);
    },
    Mission: function (data) {
        return cut(data, executor);
    }
};

module.exports = {
    Event: eventRules,
    Mission: misssionRules,
    Cause: causeRules,
    User: userRules,
    Problem: problemRules,
    cutter: cutter
};
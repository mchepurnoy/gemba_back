const mongoose = require('mongoose');
const conf = require('../config.js');
const models = require('./schemas.js');

mongoose.Promise = global.Promise;

const uri = 'mongodb://' + conf.DB.host + '/' + conf.DB.name;
mongoose.connect(uri, {
    db: {native_parser: true},
    server: {poolSize: 5}
});

const modelDeps = {
    User: null,
    Process: null,
    Event: 'creator members places.doc places.creator executors processes.doc processes.creator missions.creator missions.executors causes.creator problems.creator',
};


function checkModel(modelName) {
    return new Promise((resolve, reject) => {
        if (models[modelName]) {
            resolve();
        } else {
            reject({code: 'no_such_model'});
        }
    });
}
function isEmpty(data) {
    if (data && Object.keys(data).length) {
        return data;
    } else {
        throw {code: 'not_found'};
    }
}

function findOne(modelName, queryObj) {
    return checkModel(modelName)
        .then(() => {
            const deps = modelDeps[modelName] || '';
            return models[modelName]
                .findOne(queryObj)
                .populate(deps)
                .lean().exec();
        })
        .then(isEmpty);
}

function find(modelName,
              queryObj = {},
              limit = 20,
              sort = {createdAt: 'desc'}) {
    return checkModel(modelName)
        .then(() => {
            const deps = modelDeps[modelName] || '';
            return models[modelName]
                .find(queryObj)
                .sort(sort)
                .populate(deps)
                .limit(limit)
                .lean().exec();
        })
        .then(isEmpty);
}

function create(modelName, data) {
    return checkModel(modelName)
        .then(() => {
            return (new models[modelName](data)).save();
        });
}

function edit(modelName, queryObj, data, upsert = false) {
    return checkModel(modelName)
        .then(() => {
            return models[modelName]
                .findOneAndUpdate(
                    queryObj,
                    data,
                    //{upsert: upsert, 'new': true})
                    {runValidators: true, 'new': true})
                .then(isEmpty);
        });
}

function remove(modelName, queryObj) {
    return checkModel(modelName)
        .then(() => {
            return models[modelName]
                .remove(queryObj)
                .then(isEmpty);
        });
}

function getListsStats() {
    var P = [];
    Object.keys(modelDeps).forEach(name => {
        P.push(
            models[name].count({})
                .exec()
                .then(count => {
                    return {name: name, count: count};
                })
        );
    });
    return Promise.all(P);
}

module.exports = {
    findOne: findOne,
    find: find,
    edit: edit,
    create: create,
    remove: remove,
    getListsStats: getListsStats
};
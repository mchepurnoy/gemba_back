const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const utils = require('../helpers/utils.js');

const userSchema = Schema({
    username: {type: String, required: true, trim: true},
    password: {type: String, required: true, trim: true},
    token: {type: String, required: true, trim: true},
    expires: {type: String, required: false, trim: true},
    email: {type: String, required: false, trim: true},
    avatarUrl: {type: String, required: false, trim: true},
}, {timestamps: true});

const processSchema = Schema({
    Code: {type: String, required: true, trim: true},
    ProcessArea: {type: String, required: true, trim: true},
    Process: {type: String, required: true, trim: true},
    Block: {type: String, required: true, trim: true},
    Owner: {type: String, required: true, trim: true}
});

const eventSchema = Schema({
    title: {type: String, required: true, trim: true},
    date: {type: Date, required: true},
    places: [{
        creator: {type: Schema.ObjectId, ref: 'User', required: true},
        doc: {type: Schema.ObjectId, ref: 'Depart', required: true}
    }],
    problems: [{
        creator: {type: Schema.ObjectId, ref: 'User', required: true},
        text: {type: String, required: true, trim: true},
        title: {type: String, required: true, trim: true},
        //id: { type: String, default: utils.generateId}
    }],
    processes: [{
        creator: {type: Schema.ObjectId, ref: 'User', required: true},
        doc: {type: Schema.ObjectId, ref: 'Process', required: true}
    }],
    causes: [{
        creator: {type: Schema.ObjectId, ref: 'User', required: true},
        text: {type: String, required: true, trim: true},
        title: {type: String, required: true, trim: true},
        //id: { type: String, default: utils.generateId}
    }],
    missions: [{
        creator: {type: Schema.ObjectId, ref: 'User', required: true},
        date: {type: Date},
        description: {type: String, required: true, trim: true},
        processes: [{type: String, required: true, trim: true}],
        status: {type: String, default: 'new', trim: true},
        executors: [{type: Schema.ObjectId, ref: 'User'}],
        //id: {type: String, default: utils.generateId}
    }],
    members: [{type: Schema.ObjectId, ref: 'User'}],
    evaluation: {type: String, trim: true},
    status: {type: String, trim: true, default: 'new'},
    subdivision: {type: String, trim: true},
    description: {type: String, trim: true},
    creator: {type: Schema.ObjectId, ref: 'User', required: true}
}, {timestamps: true});

const departSchema = Schema({
    attributes: {
        OBJECTID: {type: Number, required: true},
        DEPART_NAME: String,
        DEPART_TYPE: String,
        DEPART_ADRES: String,
        DEPART_MANAGER: String,
        DEPART_TELEFON: String,
        DEPART_OFFWDATE: String,
        DEPART_OFFWTIME: String,
        DEPART_REFORMATD: String,
        CLASS_ITOG: String
    },
    geometry: {
        x: {type: Number, required: true},
        y: {type: Number, required: true}
    }
});

var Depart = mongoose.model('Depart', departSchema);
var Process = mongoose.model('Process', processSchema);
var Event = mongoose.model('Event', eventSchema);
var User = mongoose.model('User', userSchema);

module.exports = {
    User: User,
    Depart: Depart,
    Process: Process,
    Event: Event
};


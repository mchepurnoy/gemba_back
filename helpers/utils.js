const _ = require('lodash');

function generateId() {
    var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+S4()+"-"+S4()+S4()+S4());
}

function removePrivateData (user) {
    return _.omit(user, ['_id', 'token', 'expires']);
}

function getCount (arr) {
    return (arr && arr.length) ? arr.length : 0;
}

module.exports = {
    generateId: generateId,
    removePrivateData: removePrivateData,
    getCount: getCount
};
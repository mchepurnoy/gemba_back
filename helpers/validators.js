//const Joi = require('joi');
//const dbs = require('../db/service.js');
const _ = require('lodash');
//const filters = require('./filters.js');

//чому-то не работает, долго разбираться
// const eventFilter = Joi.object().keys({
//     startDate: Joi.number(),
//     endDate: Joi.number(),
//     place: Joi.string().empty(""),
//     executor: Joi.string().empty("")
// }).without('startDate', 'endDate', 'place', 'executor');
//
function findRoleArr (arr, token) {
    var permObj = {};
    arr.forEach(el => {
        if (el.creator && el.creator.token == token) {
            permObj[el._id.toString()] = 'creator';
            return;
        }
        if (el.executors) {
            el.executors.forEach(ex => {
                if (ex.token == token) {
                    permObj[el._id.toString()] = 'executor';
                }
            })
        }
    });
    return permObj;
}
function findRole(item, token) {
    var permissions = {
        event: '',
        missions: {},
        causes: {},
        problems: {},
        places: {}
    };
    if (item.creator && item.creator.token == token) {
        permissions.event = 'creator';
    }
    if (item.missions) {
        permissions.missions = findRoleArr(item.missions, token);
    }
    if (item.problems) {
        permissions.problems = findRoleArr(item.problems, token);
    }
    if (item.causes) {
        permissions.causes = findRoleArr(item.causes, token);
    }
    if (item.places) {
        permissions.places = findRoleArr(item.places, token);
    }
    if (item.processes) {
        permissions.processes = findRoleArr(item.processes, token);
    }
    if (item.members && item.members.length) {
        for (let i in item.members) {
            if (item.members[i].token == token && permissions.event != 'creator') {
                permissions.event = 'member';
                break;
            }
        }
    }
    return permissions;
}

const memberCanEdit = ['description', 'missions', 'causes', 'processes', 'problems', 'places'];
const memberNested = ['causes', 'problems', 'missions'];
const memberRefs = ['places', 'processes'];
function getEditorPermissions (item, body, token) {
    const permissions = findRole(item, token);
    var edition = _.pick(body, memberCanEdit);
    var del = [];//console.log(permissions);
    memberNested.forEach(key => {
        if (item[key] && item[key].length) {
            item[key] = item[key].filter(el => {
                if (permissions[key]) {
                    return permissions[key][el._id.toString()] == 'creator';
                } else {
                    return false;
                }
            });
            if (!item[key].length) {
                del.push(key);
            }
        }
    });
    memberNested.forEach(key => {
        if (body[key] && body[key].length) {
            body[key] = body[key].filter(el => {
                if (permissions[key]) {
                    return permissions[key][el._id.toString()] == 'creator';
                } else {
                    return false;
                }
            });
        }
    });
    return _.omit(edition, del);
}
// function filterEditable (item, permissions, type) {
//     if (item[type].length) {
//         item[type] = item[type].filter(el => {
//             return permissions[el._id] == 'creator';
//         });
//         if (!item.length) {
//
//         }
//     }
// }

module.exports = {
    //getEventFilterQuery: getEventFilterQuery,
    findRole: findRole,
    getEditorPermissions: getEditorPermissions
};
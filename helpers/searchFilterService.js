const dbs = require('../db/service.js');
//const _ = require('lodash');

const searchFilters = {
    depart: function (text) {
        const filter = {$regex: '.*' + text + '.*', $options: 'i'};
        return {$or: [
            {"attributes.DEPART_NAME" : filter},
            {"attributes.DEPART_TYPE" : filter},
            {"attributes.DEPART_ADRES" : filter},
            {"attributes.DEPART_MANAGER" : filter},
            {"attributes.DEPART_TELEFON" : filter}
        ]};
    },
    process: function (text) {
        const filter = {$regex: '.*' + text + '.*', $options: 'i'};
        return {$or: [
            {"Code" : filter},
            {"ProcessArea" : filter},
            {"Process" : filter},
            {"Block" : filter},
            {"Owner" : filter}
        ]};
    },
    user: function (text) {
        return {username: {$regex: '.*' + text + '.*', $options: 'i'}};
    }
};


function getEventFilterQuery(data, callback) {
    var dateFilter = {};
    var q = {};
    if (data.startDate) {
        dateFilter['$gte'] = data.startDate;//new Date(data.startDate);
    }
    if (data.endDate && data.endDate > data.startDate) {
        dateFilter['$lt'] = data.endDate;//new Date(data.endDate);
    }
    if (Object.keys(dateFilter).length) {
        q.date = dateFilter;
    }
    if (data.executor || data.process || data.place) {
        var P = [];
        if (data.executor) {
            P.push(dbs.find('User', searchFilters.user(data.executor)));
        }
        if (data.process) {
            P.push(dbs.find('Process', searchFilters.process(data.process)))
        }
        if (data.place) {
            P.push(dbs.find('Depart', searchFilters.depart(data.place), 1000));
        }
        Promise.all(P)
            .then(results => {
                for (let i in results) {
                    if (results[i] && results[i].length) {
                        if (results[i][0]) {
                            //--------------------------------------------
                            if (results[i][0].Code) {
                                q.processes = {
                                    "$elemMatch": {
                                        "doc": {"$in": results[i].map(d => {return d._id;})}
                                    }
                                };
                            } else if (results[i][0].username) {
                                q.missions = {
                                    "$elemMatch": {
                                        "executors": {"$in": results[i].map(d => {return d._id;})}
                                    }
                                };
                            } else if (results[i][0].attributes) {
                                q.places = {
                                    "$elemMatch": {
                                        "doc": {"$in": results[i].map(d => {return d._id;})}
                                    }
                                };
                            }
                            //--------------------------------------------
                        }
                    }
                }
                //console.log(q);
                callback(q);
            })
            .catch(ex => {
                callback({error: ex});
            });
    } else {
        callback(q);
    }
}

module.exports = {
    getEventFilterQuery: getEventFilterQuery,
    searchFilters: searchFilters
};
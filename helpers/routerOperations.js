//const db = require('./../services/DBService.js');
const dbs = require('../db/service.js');

//headers for CORS
const allow_headers = ['Origin', 'X-Requested-With', 'Content-Type', 'Accept',
    'Access-Control-Allow-Credentials', 'Accept-Encoding', 'Accept-Language',
    'Cache-Control', 'Connection', 'Content-Length', 'Cookie', 'Host', 'User-Agent',
    'Connection', 'Referer', 'Accept', 'User-Agent', 'Pragma', 'Upgrade-Insecure-Requests',
    'gistoken'];

function setCORSReq (req, res ,next) {
    var origin = req.get('origin');
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.header("Access-Control-Allow-Headers", allow_headers.join(', '));
    res.header('Content-Type', 'application/json; charset=UTF-8');
    next();
}

function getCookieParam (req, param) {
    if (req.cookies && req.cookies[param]) {
        return req.cookies[param];
    }
    return null;
}

function getError(errMsg) {
    return {
        error: errMsg
    }
}

function jsonParse (req, res, next) {
    if (req.method == 'POST' || req.method == 'PUT') {
        var data = '';
        req.setEncoding('utf8');
        req.on('data', (chunk) => {
            data += chunk;
        });
        req.on('end', () => {
            try {
                req.body = JSON.parse(data);
                next();
            } catch (e) {
                res.json(getError('invalid json'));
            }
        });
    } else {
        next();
    }
}

function sessionCheck (req, res, next) {
    //const token = getCookieParam(req, 'gis_sid');
    const token = req.get('gistoken');
    if (req.method === 'OPTIONS') {
        next();
    } else {
        if (token) {
            dbs.findOne('User', {token: token}).then(
                (data) => {
                    if (data) {
                        req.user = data;
                        next();
                    } else {
                        res.json({err: 'invalid token'});
                    }
                },
                (data) => {
                    res.json(data || getError('invalid token'));
                });
        } else {
            res.json(getError('invalid token'))
        }
    }
}

function makeParamsCheck (params) {
    return function (req, res, next) {
        var valid = true;
        for (var i in params) {
            if (!req.params[params[i]]) {
                valid = false;
                break;
            }
        }
        if (valid) {
            next();
        } else {
            res.json({err: 'invalid params'})
        }
    }
}

module.exports = {
    setCORSReq: setCORSReq,
    getError: getError,
    jsonParse: jsonParse,
    getCookieParam: getCookieParam,
    sessionCheck: sessionCheck
};

// var o ={"where": "DEPART_ADRES like Тульская",
// "geometryType": "esriGeometryEnvelope", "spatialRel":"esriSpatialRelIntersects",
// "outFields":"*","returnGeometry":true,"returnTrueCurves":false,
// "returnIdsOnly":"false","returnCountOnly":false,"f":"pjson"};
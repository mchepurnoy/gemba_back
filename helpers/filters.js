const _ = require('lodash');

function removePrivateData (user) {
    return _.omit(user, ['token', 'expires', 'password']);
}

function filterUsersList (users) {
    return users.map(u => {
        return removePrivateData(u);
    })
}

function removeUsers(arr) {
    arr.forEach(el => {
        if (el.creator) {
            el.creator = removePrivateData(el.creator);
        }
        if (el.executors) {
            el.executors = el.executors.map(removePrivateData);
        }
    });
}

function autoFilter (obj) {
    if (obj.creator) {
        obj.creator = removePrivateData(obj.creator);
    }
    if (obj.members) {
        obj.members = obj.members.map(removePrivateData);
    }
    // if (obj.executor) {
    //     obj.creator = removePrivateData(obj.executor);
    // }
    if (obj.executors) {
        obj.executors = obj.executors.map(removePrivateData);
    }
    //
    if (obj.causes) {
        removeUsers(obj.causes);
    }
    if (obj.problems) {
        removeUsers(obj.problems);
    }
    if (obj.missions) {
        removeUsers(obj.missions);
    }
    return obj;
}
function autoFilterList(arr) {
    return arr.map(el => {
        return autoFilter(el);
    });
}

function addCreator (obj, user) {
    obj.creator = user._id;
    return obj;
}

function makeQueryObj (obj) {
    return obj;
}

function filterUsers(arr) {
    return arr.map(u => {
        return removePrivateData(u);
    });
}

const Event = {
    read: function (obj) {
        obj.creator = removePrivateData(obj.creator);
        obj.members = obj.members.map(removePrivateData);
        return obj;
    },
    list: function (arr) {
        var arrFiltered = arr;
        arrFiltered.forEach((el) => {
            _.omit('members', 'creator', 'problems', 'files');
        });
        Object.assign(arrFiltered, {
            stats: {
                members: getCount(arr.members),
                problems: getCount(arr.problems),
                files: getCount(arr.files)
            }
        });
        return arrFiltered;
    }
};

module.exports = {
    event: Event,
    autoFilter: autoFilter,
    makeQueryObj: makeQueryObj,
    addCreator: addCreator,
    filterUsersList: filterUsersList,
    autoFilterList: autoFilterList,
    filterUsers: filterUsers
};
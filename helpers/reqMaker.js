const http = require('http');

const Headers = {
    'Pragma':'no-cache',
    'Accept-Language':'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    'Content-Type':'application/json',
    'Cache-Control': 'no-cache'
};

function makeReq (data, path, options = {}) {
    return new Promise((resolve, reject) => {
        const reqOptions = Object.assign({}, {
            host: 'localhost',
            port: 3013,
            path: path,
            method: 'POST',
            headers: Headers
        }, options);
        var chunks = [];
        var post_req = http.request(reqOptions, function(res) {
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });
            res.on('end', () => {
                resolve(Buffer.concat(chunks));
            });
            res.on('error', (err) => {
                reject(err);
            });
        });
        post_req.write(JSON.stringify(data));
        post_req.end();
    });
}

module.exports = {
    makeReq: makeReq
};
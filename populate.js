const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const conf = require('./config.js');
const models = require('./db/schemas.js');
const departs = require('./sample_data/departs.js');
const _ = require('lodash');

const procs = require('./sample_data/processes.js');

const uri = 'mongodb://' + conf.DB.host + '/' + conf.DB.name;
mongoose.connect(uri, {
    db: {native_parser: true},
    server: {poolSize: 2}
});

function insertMany (data, type, callback) {
    models[type].insertMany(data, (err, docs) => {
        if (err) {
            console.log(err);
        } else {
            console.log(docs.length + ' ' + type +' added');
            if (callback) {
                callback();
            }
        }
    });
}

function insertDeparts () {
    /*models.Depart.insertMany(data.features, (err, docs) => {
     if (err) {
     console.log(err);
     } else {
     console.log(docs.length + 'departs added');
     }
     });*/
    //insertMany(data.features, 'Depart');
}

var users = [
    {
        //"_id" : ObjectId("57ee766d3c903cac1360d081"),
        "username" : "user1",
        "password": "qwerty123",
        "expires" : "1476193736152",
        "token" : "jgV72xBH0GjrtGwkydGeGZ7YfxNW3g6KvFyLrkMxCek."
    },
    {
        //"_id" : ObjectId("57f503ff3e5a8b8e5a50e698"),
        "username" : "user2",
        "expires" : "1475680893629",
        "password": "qwerty123",
        "token" : "y7E_rpOT37zDYTiuuN66-wrB0K5KPDUz-51IpORuxsQ."
    },
    {
        //"_id" : ObjectId("57f5049d3e5a8b8e5a50e699"),
        "username" : "user3",
        "expires" : "1475759693947",
        "password": "qwerty123",
        "token" : "eKwt01py8nygEW48EgzZPu8FUpYCFPZjU4FvgZL8fMk."
    },
    {
        //"_id" : ObjectId("57f5049d3e5a8b8e5a50e699"),
        "username" : "user4",
        "expires" : "1475759693947",
        "password": "qwerty123",
        "token" : "eKwt01py8nygEW48EgzZPu8FUpYCFPZjU4FvgZL8zzz."
    },
    {
        //"_id" : ObjectId("580606668c74c96008ae6547"),
        "username" : "test1",
        "expires" : "1475759693947",
        "password": "qwerty123",
        "token" : "fKwt01py8nygEW48EgzZPu8FUpYCFPZjU4FvgZL8sss.",
    },
    {
        //"_id" : "580606898c74c96008ae6548",
        "username" : "test2",
        "expires" : "1475759693947",
        "password": "qwerty123",
        "token" : "fKwt01py8nygEW48EgzZPu8FUpYCFPZjU4FvgZL8fgf."
    },
    {
        //"_id" : "580606898c74c96008ae6548",
        "username" : "test3",
        "expires" : "1475759693947",
        "password": "qwerty123",
        "token" : "fKwt01py8nygEW48EgzZPu8FUpYCFPZjU4FvgZL9qqq."
    }
];

var event = {
    "_id" : "5808e92cdcb2d933d753b84b",
    "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget velit nisl. Nulla semper velit vitae libero faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget velit nisl. Nulla semper velit vitae libero faucibus.",
    "creator" : "57ee766d3c903cac1360d081",
    "members" : [
        //"57ee766d3c903cac1360d081",
        //"57f5049d3e5a8b8e5a50e699"
    ],
    "problems" : [
        //"58062b2cbecf48309f3ca13e",
        //"5805f1f5082fee204a163513"
    ],
    "files" : [],
    "params" : {
        "title" : "Какое-то событие",
        "status" : "new"
    },
    "address" : "582304204614522864a012e5"
};

var event = {
    "title": "new event",
    "date": 1480679188015,
    "places": ["582304204614522864a012df", "582304204614522864a012e5"],
    "processes": ["5840258b767a2b6572e2de34", "5840258b767a2b6572e2de3d"],
    "problems": [{"text":"описание проблемы", "title":"заголовок проблемы"},{"text":"описание проблемы", "title":"заголовок проблемы"}],
    "causes": [{"text":"описание приичны", "title":"заголовок причины"},{"text":"описание причины", "title":"заголовок причины"}],
    "missions": [{"creator": "584046dde1ed1338e25b7be2", "date":1480679188015, "processes":["У.4.5"], "description":"fewf efewfewf"}],
    //"executors":["584046dde1ed1338e25b7be4", "584046dde1ed1338e25b7be3"],
    "subdivision":"jfljfe ewefewf ewf ejlj",
    "evaluation":"",
    "creator": "584046dde1ed1338e25b7be2",
    "description": "описание"
};


function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

var u=0;
function getUser(users) {
    if (u < users.length) {
        var usr = users[u]._id.toString();
        u++;
        return usr;
    } else {
        u=0;
        return users[0]._id.toString();
    }
}

function makeMission(proc, creator, exec) {
    return {
        "creator" : creator,
        "date" : randomDate(new Date(2016, 11, 1), new Date()),
        "description" : "fewf efewfewf",
        "processes" : [
            proc
        ],
        executors: [exec]
    };
}
function makeCause(creator) {
    return {
        "text" : "описание причины",
        "title" : "заголовок причины",
        "creator": creator
    };
}
function makeProblem(creator) {
    return {
        "text" : "описание причины",
        "title" : "заголовок причины",
        "creator": creator
    };
}

function randomgemba() {
    var dnums = [];
    for (var i=100;i<120;i++) {
        dnums.push(i);
    }
    var evs = [];
    models.Depart.find({}, {_id:1}, (err, data) => {
        if (!err) {
            models.Process.find({}, (errp, datap) => {
                if (!errp) {
                    models.User.find({}, (erru, usersd) => {
                        if (!erru) {
                            dnums.forEach(n => {
                                var ev = _.cloneDeep(event);
                                ev.name += ' ' + n;
                                ev.places = {doc: [data[n]._id], creator: getUser(usersd)};
                                ev.processes = {doc:[datap[n - 20]._id.toString()],creator:getUser(usersd)};
                                //ev.executors = [getUser(usersd)];
                                ev.creator = getUser(usersd);
                                ev.members = [getUser(usersd),getUser(usersd)];
                                ev.missions = [makeMission(datap[n - 20].Code, getUser(usersd), getUser(usersd))];
                                ev.problems = [makeProblem(getUser(usersd))];
                                ev.causes = [makeCause(getUser(usersd))];
                                //console.log(ev);
                                evs.push(ev);
                            });
                            //console.log(evs[0]);
                            insertMany(evs, 'Event');
                        }
                    });
                }
            });
        }
    });
}

//console.log(departs.features[0])
//insertMany(departs.features, 'Depart');
//insertMany(users, 'User', () => {randomgemba();});
//insertMany([event], 'Event');
//insertMany(procs.processes, 'Process');

//db.getCollection('departs').createIndex({"attributes.DEPART_ADRES": "text"});
//db.getCollection('departs').find({$text: {$search: "Тульская"}})
//randomgemba();